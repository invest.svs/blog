import Post from "./post.js"
import posts from './posts.json'

function Posts() {
    const posts_list = posts.map(post=>{
      return <Post key={post.id} title={post.title} body={post.body}/>
    })
     return (
       <div className="App">
         {posts_list}
         <HelloWorld text = "Привет!" color="red"/>
         
   
       </div>
     );
   }

   export default Posts;